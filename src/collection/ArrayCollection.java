package collection;

import java.util.Collection;
import java.util.Iterator;

import static java.util.Objects.isNull;

/* Collection
 * - In order data
 * - Could contain same data
 */
public class ArrayCollection<E> implements Collection<E> {

  private transient Object[] elementData;
  private int size;

  public static void main(String[] args) {
    ArrayCollection<Integer> nums = new ArrayCollection<>(2);
    nums.add(3);
    nums.add(5);
    nums.add(7);
    System.out.println(nums.contains(6));

    Object[] numArr = nums.toArray();

    System.out.println("length of numArr: " + numArr.length);

    for (Object num: numArr) {
      System.out.println(num);
    }

  }

  // No init capacity
  public ArrayCollection() {
    // SET default capacity for array
    elementData = new Object[10];
  }

  public ArrayCollection(int initCapacity) {
    elementData = new Object[initCapacity];
  }

  @Override
  public boolean add(E e) {
    if (isNull(e)) throw new IllegalArgumentException("New element must not be null!");
    elementData[size++] = e;
    ensureCapacity(size + 1);
    return true;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  public int indexOf(Object o) {
    for (int index = 0; index < size; index++)
      if (elementData[index].equals(o))
        return index;

    return -1;
  }

  public boolean contains(Object o) {
    return indexOf(o) != -1;
  }

  @Override
  public boolean remove(Object o) {
    int index = indexOf(o);
    if (index != -1) {
      // Take last index value of elements instead removal element
      elementData[index] = elementData[--size/*decrement size*/];
      elementData[size] = null; // SET last element is null
    }
    return false;
  }

  // Expand capacity when array out of bound.
  private void ensureCapacity(int capacity) {
    if (capacity > elementData.length) {
      int newCapacity = Math.max(capacity, elementData.length * 2);
      Object[] arr = new Object[newCapacity];

      for (int index = 0; index < size; index++) {
        arr[index] = elementData[index];
      }
      elementData = arr;
    }
  }

  // convert from ArrayCollection to array for access data in collection.
  @Override
  public Object[] toArray() {
    Object[] arr = new Object[size];
    for (int index = 0; index < size; index++)
      arr[index] = elementData[index];

    return arr;
  }

  @Override
  public Iterator iterator() {
    return null;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }
}
